import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {ParallaxProvider} from "react-scroll-parallax/cjs";

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import './assets/css/main.css';


ReactDOM.render(
    <ParallaxProvider>
        <App/>
    </ParallaxProvider>,
    document.getElementById('root')
);

