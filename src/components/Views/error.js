import React, {Component} from 'react';
import {Grid} from 'react-flexbox-grid';

import Header from '../Header/index';



/**
 * Component page used to display errors.
 */
class Error extends Component {

    render() {
        return (
            <Grid fluid>
                <Header title={'ERROR 404 - Not found'}/>

                <h1>Oops... Het lijkt erop dat de pagina die u zoekt niet bestaat.</h1>

            </Grid>
        );
    }
}

export default Error;
