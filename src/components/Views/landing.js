import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-flexbox-grid';

import Header from '../Header'
import {LandingHero} from '../Layout/index';

import {Mousey} from '../Animations/index';

/**
 * Landing page
 */
class Landing extends Component {

    render() {
        return (
            <Grid fluid>

                <Header hasHero={true}/>


                <Row center={'xs'} className={'hero-wrapper'}>
                    <LandingHero />
                </Row>

                <div className={'kifesh'}>
                </div>


                <Mousey/>

            </Grid>
        );
    }
}

export default Landing;
