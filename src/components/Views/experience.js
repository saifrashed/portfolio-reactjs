import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-flexbox-grid';


import Header from '../Header/index';
import Timeline from '../Timeline/index';
import {ExperienceHero} from '../Layout/index';


/**
 * Experience Saif page
 */
class Experience extends Component {

    render() {
        return (
            <Grid fluid className={'content'}>
                <Header hasHero={true} title={'Tijdlijn'}/>

                <Row center={'xs'} className={'hero-wrapper'}>
                    <ExperienceHero/>
                </Row>

                <Row>
                    <Timeline/>
                </Row>
            </Grid>
        );
    }
}

export default Experience;
