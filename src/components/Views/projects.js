import React, {Component} from 'react';
import {Grid} from 'react-flexbox-grid';


import Header from '../Header/index';
import Posts from "../Posts/index";


/**
 * Component page used to display Projects.
 */
class Projects extends Component {

    render() {
        return (
            <Grid fluid>
                <Header title={'Projecten'}/>

                <Posts posttype={'projects'} slug={this.props.match.params.postId}/>
            </Grid>
        );
    }
}

export default Projects;
