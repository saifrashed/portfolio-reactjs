import React, {Component} from 'react';
import {Col, Grid, Row} from "react-flexbox-grid";
import $ from 'jquery';

import Footer from '../Footer/index';


/**
 * Menu component responsible for displaying top menu
 */
class Menu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen:     false,
            colorShift: true,
        };

        this.toggleMenu = this.toggleMenu.bind(this);
        this.link       = this.link.bind(this);
        this.colorShift = this.colorShift.bind(this);
    }

    componentDidMount() {
        if (this.props.hasHero) {
            $('.logo').addClass('logo-white');
            $('.bar').addClass('bar-white');

            window.addEventListener('scroll', this.colorShift);
        }
    }

    /**
     * Menu toggle by adding/removing class
     */
    toggleMenu = () => {
        if (this.state.isOpen === false) {
            $('body').css('overflow', 'hidden');
            $('.menu').addClass('menu-open');
            $('.toggle').addClass('clicked');
            $('.logo').addClass('logo-white');

            this.setState({isOpen: true});
        } else {
            if (this.state.colorShift && this.props.hasHero) {
                $('body').css('overflow', 'auto');
                $('.menu').removeClass('menu-open');
                $('.toggle').removeClass('clicked');
                this.setState({isOpen: false});
            } else {
                $('body').css('overflow', 'auto');
                $('.menu').removeClass('menu-open');
                $('.toggle').removeClass('clicked');
                $('.logo').removeClass('logo-white');
                this.setState({isOpen: false});
            }
        }
    };

    /**
     * Handles menu bar color shifts in landing
     * note: only gets used when hasHero prop is true.
     */
    colorShift = () => {

        var heroHeight = $('.hero').height();

        var pageOffset = window.pageYOffset;
        var heroOffset = $('.hero').offset();

        var shiftBreakPoint = heroOffset.top + heroHeight;

        this.setState({colorShift: false});

        if (pageOffset > shiftBreakPoint && this.state.colorShift === false) {

            this.setState({colorShift: false});

            $('.logo').removeClass('logo-white');
            $('.bar').removeClass('bar-white')
        } else {

            this.setState({colorShift: true});

            $('.logo').addClass('logo-white');
            $('.bar').addClass('bar-white');
        }
    };

    /**
     * Links close the menu before redirecting
     * @param e
     */
    link = e => {

        e.preventDefault();

        var linkTo = e.target.getAttribute('href');
        var menu   = $('.menu');
        var toggle = $('.toggle');
        var logo   = $('.logo');

        menu.removeClass('menu-open');
        toggle.removeClass('clicked');
        logo.removeClass('logo-white');

        setTimeout(function () {
            window.location.href = linkTo;
        }, 500);
    };


    render() {
        return (
            <div>
                <div className="toggle" onClick={this.toggleMenu}>
                    <div className="bar"></div>
                </div>

                <div className={"menu"}>
                    <nav>
                        <ul className={'menu-list'}>
                            <li>
                                <a href="/projecten" onClick={this.link}>PROJECTEN</a>
                            </li>
                            <li>
                                <a href="/ervaring" onClick={this.link}>MIJN TIJDLIJN</a>
                            </li>
                            <li>
                                <a href="/contact" onClick={this.link}>CONTACT</a>
                            </li>
                        </ul>
                    </nav>
                    <Footer/>
                </div>
            </div>
        );
    }
}

export default Menu;
