import React from 'react';
import {Col, Grid, Row} from "react-flexbox-grid";


// Import assets

import {Parallax} from "react-scroll-parallax";
import Image from "../../assets/img/landing_banner.jpg";
import HeroPortrait from "../../assets/img/hero_portret.jpg";


/**
 * Hero used in landing view
 * @returns {*}
 * @constructor
 */
export function LandingHero() {
    return (
        <Col xs={12}  md={12} className={'hero'}>

            <Col xs={12} md={6} className={'hero-text'}>
                <div>
                    <span id={'greeting'}>Hallo.</span>
                    <h1 id={'heading'}>Ik ben Saif Rashed</h1>
                    <span id={'function'}>Software ontwikkelaar</span>
                    <a href={'/projecten'}><span id={'call-to-action'}>
                        Bekijk mijn werk
                        <i className="fa fa-arrow-right"></i>
                    </span>
                    </a>
                </div>
            </Col>

            <Col xs={12} md={6} className={'hero-image'}>
                <img src={HeroPortrait}/>
            </Col>
        </Col>
    );
}

/**
 *  Hero used in experience view
 * @returns {*}
 * @constructor
 */
export function ExperienceHero() {
    return (
        <Col xs={12}  md={12} className={'hero'}>

            <Col xs={12} md={6} className={'hero-text'}>
                <div>
                    <h1 id={'heading'}>Mijn Tijdlijn</h1>
                </div>
            </Col>

            <Col xs={12} md={6} className={'hero-image'}>
                <img src={HeroPortrait}/>
            </Col>
        </Col>
    );
}
