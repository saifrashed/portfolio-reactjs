import React, {Component} from 'react';


import Menu from '../Menu/index';
import Logo from '../../assets/img/logo.png';

/**
 * Menu component responsible for displaying top menu
 */
class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {};

    }

    componentDidMount() {
        // Sets page title for SEO
        if (this.props.title) {
            document.title = 'Saif Rashed' + ' - ' + this.props.title;
        } else {
            document.title = 'Saif Rashed';
        }
    }

    render() {
        return (
            <header>

                <div className={"logo"}>
                    <a href={'/'}>
                        <img src={Logo} alt={'Saif Rashed Logo'}/>
                    </a>
                </div>

                <Menu hasHero={this.props.hasHero} />
            </header>
        );
    }
}

export default Header;
