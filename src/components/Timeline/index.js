import React, {Component} from 'react';
import axios from 'axios';

import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import {Roller} from '../Animations';

import PostAddIcon from '@material-ui/icons/PostAdd';
import SchoolIcon from '@material-ui/icons/School';
import WorkIcon from '@material-ui/icons/Work';
import StarIcon from '@material-ui/icons/Star';

/**
 * Timeline listing all my experiences in chronological order
 */
class Timeline extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded:    false,
            projects:    [],
            experiences: [],
            tags:        [],
        };


    }


    componentDidMount() {
        axios.get('http://admin.saifrashed.nl/wp-json/wp/v2/projects') // Requesting relevant post types.
             .then(res => {
                 this.setState({
                     projects: res.data,
                 });

                 axios.get('http://admin.saifrashed.nl/wp-json/wp/v2/experiences') // Requesting relevant post types.
                      .then(res => {
                          this.setState({
                              experiences: res.data,
                          });

                          axios.get('http://admin.saifrashed.nl/wp-json/wp/v2/tags') // Requesting all tags.
                               .then(res => {
                                   this.setState({
                                       isLoaded: true,
                                       tags:     res.data,
                                   })
                               });
                      });
             });


    }

    // Parses WP REST date format to dutch standard
    parseDate = (string) => {

        var parsedDate = Date.parse(string);
        var newDate = new Date(parsedDate);

        return newDate.getDate() + '-' + newDate.getMonth() + '-' + newDate.getFullYear();

        console.log(newDate.getDate() + '-' + newDate.getMonth() + '-' + newDate.getFullYear())
    };

    arrayConcat = () => {

        var projects = this.state.projects;
        var experiences = this.state.experiences;

        var allExperiences = projects.concat(experiences);

        return allExperiences;

    };

    timelineContent = () => {
        return (this.arrayConcat().map(item => (
                <VerticalTimelineElement
                    key={item.id}
                    className="vertical-timeline-element--work"
                    date={this.parseDate(item.date)}
                    iconStyle={{background: 'rgb(255,255,255)', color: '#000'}}
                    icon={<WorkIcon/>}
                >
                    <h3 className="vertical-timeline-element-title">{item.title.rendered}</h3>
                    <p>
                        {item.content.rendered.replace(/(<([^>]+)>)/ig, "")}
                    </p>
                </VerticalTimelineElement>
        )))
    };

    test = () => {
        console.log(this.state.projects);
        console.log(this.state.experiences);
        console.log(this.state.tags);
        console.log(this.state.isLoaded);
    };

    render() {
        var {isLoaded, items} = this.state;

        if (!isLoaded) { // Loading display
            return (<Roller/>)
        } else {
            return (
                <VerticalTimeline>

                    {this.timelineContent()}

                    <VerticalTimelineElement
                        iconStyle={{background: 'rgb(255,255,255)', color: '#000'}}
                        icon={<StarIcon/>}
                    />

                    {this.test()}
                </VerticalTimeline>
            );
        }
    }
}

export default Timeline;
