import React, {Component} from 'react';
import {Grid} from "react-flexbox-grid";


export function Roller(props) {
    return <div className={'loading-animation'}>
        <div className="roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
}

export function Mousey(props) {
    return (
        <div className={"scroll-downs"}>
            <div className={"mousey"}>
                <div className={"scroller"}></div>
            </div>
        </div>
    );
}
