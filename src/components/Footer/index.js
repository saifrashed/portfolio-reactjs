import React, {Component} from 'react';
import {Col, Row} from "react-flexbox-grid";

/**
 * Footer component responsible for displaying bottom text
 */
class Footer extends Component {

    render() {
        return (
            <footer>
                <Row center={'xs'}>
                    <Col xs={12} md={12}>
                        <small>&copy; Copyright 2018 - {new Date().getFullYear()}, Saif Rashed</small>
                    </Col>
                </Row>
            </footer>
        );
    }
}

export default Footer;
