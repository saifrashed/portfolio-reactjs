import React, {Component} from 'react';
import {Row, Col} from 'react-flexbox-grid';
import axios from 'axios';

import ImageGallery from 'react-image-gallery';
import {Roller} from '../Animations';
import PublicIcon from '@material-ui/icons/Public';
import CodeIcon from '@material-ui/icons/Code';

const fetchUrl = 'http://admin.saifrashed.nl/wp-json/wp/v2/'; // URL api

/**
 * Fetches post data from WP REST API and processes it to display.
 */
class Posts extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tags:     [],
            items:    [],
            slug:     this.props.slug,
            isLoaded: false,
            postType: this.props.posttype,
        };
    }


    componentDidMount() {
        axios.get(fetchUrl + this.state.postType) // Requesting relevant post types.
             .then(res => {
                 this.setState({
                     isLoaded: true,
                     items:    res.data,
                 })
             });

        axios.get(fetchUrl + 'tags') // Requesting all tags.
             .then(res => {
                 this.setState({
                     tags: res.data,
                 })
             });
    }


    /**
     * Maps post links
     * @param links
     */
    mapLinks = (links) => {

        if (links) {
            return (links.map(item => (
                <a className={'link-box'} href={item.url} key={item}>
                    {item.icon}
                </a>
            )))
        }

        return false;
    };
    /**
     * maps tag names for associated post.
     * @param postTags
     */
    mapTags  = (postTags) => {

        var index;
        var associatedTags = [];
        var display        = '';

        if (postTags.length) {
            for (index = 0; index < this.state.tags.length; index++) {
                if (postTags.includes(this.state.tags[index].id)) {
                    associatedTags.push(this.state.tags[index].name);
                }
            }
        }

        if (associatedTags.length) {
            for (index = 0; index < associatedTags.length; index++) {
                display += associatedTags[index] + ', ';
            }

            return (associatedTags.map(item => (
                <span className={'tag'} key={item}>
                    {item}
                </span>
            )))
        }

        return false;
    };

    /**
     * Returns project archive content
     *
     * @param items
     * @returns {*}
     */
    postArchive = (items) => {
        return (items.map(item => (
            <div className={'col-xs-12 col-md-6 archives-item'} key={item.id}>
                <a href={"/projecten/" + item.slug}>
                    <img src={item.acf.thumbnail} alt={item.title.rendered}/>
                </a>

                <a href={"/projecten/" + item.slug}><h2>{item.title.rendered}</h2></a>
            </div>
        )))
    };


    /**
     * Returns project single content
     *
     * @param items
     * @param slug
     * @returns {*}
     */
    postSingle(items, slug) {
        var result;
        var galleryDisplay;
        var images = [];
        var links  = [];

        // determines single project
        for (var i = 0; i < this.state.items.length; i++) {
            if (this.state.items[i].slug === slug) {
                result = this.state.items[i];
            }
        }

        // set default image to thumbnail
        images.push({
            original:  result.acf.thumbnail,
            thumbnail: result.acf.thumbnail
        });

        if (result.acf.images) {
            for (var index = 0; index < result.acf.images.length; index++) {
                var image = result.acf.images[index];

                images.push({
                    original:  image.url,
                    thumbnail: image.url
                });
            }

            galleryDisplay = <ImageGallery items={images} showNav={false} showPlayButton={false}/>;
        } else {
            galleryDisplay = <ImageGallery items={images} showNav={false} showPlayButton={false} showThumbnails={false}/>;
        }


        if (result.acf.repository_url) {
            links.push({icon: <CodeIcon/>, url: result.acf.repository_url});
        }
        if (result.acf.url) {
            links.push({icon: <PublicIcon/>, url: result.acf.url});
        }

        if (slug)
            return (
                <div className={'single-project'}>
                    <Col className={'single-gallery'} xs={12} md={6}>
                        {galleryDisplay}
                    </Col>

                    <Col className={'single-content'} xs={12} md={6}>
                        <h1>{result.title.rendered}</h1>
                        <div className={'single-tags'}>{this.mapTags(result.tags)}</div>
                        <p>{result.content.rendered.replace(/(<([^>]+)>)/ig, "")}</p>
                        <hr></hr>
                        <div className={'single-links'}>{this.mapLinks(links)}</div>
                    </Col>
                </div>
            );
    }


    displayProjects = (items) => {
        if (this.state.slug) {
            return (
                <Row>
                    {this.postSingle(items, this.state.slug)}
                </Row>
            );
        } else {
            return (
                <Row center={'xs'}>
                    <Col className={'archives'} xs={12} md={8}>
                        {this.postArchive(items)}
                    </Col>
                </Row>
            );
        }
    };

    render() {
        var {isLoaded, items} = this.state;

        if (!isLoaded) { // Loading display
            return (<Roller/>)
        } else {
            return (
                this.displayProjects(items)
            )
        }
    }
}

export default Posts;
