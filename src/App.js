import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch, History} from "react-router-dom";

import Landing from "./components/Views/landing";
import Projects from "./components/Views/projects";
import Experience from "./components/Views/experience";
import Contact from "./components/Views/contact";
import Error from "./components/Views/error";


class App extends Component {

    render() {
        return (
            <div className="App">
                <Router>
                        <Switch>
                            <Route exact path="/" component={Landing}/>
                            <Route path={'/projecten/:postId?'} component={Projects}/>
                            <Route path={'/ervaring'} component={Experience}/>
                            <Route path={'/contact/:postId?'} component={Contact}/>
                            <Route component={Error}/>
                        </Switch>
                </Router>
            </div>
        );
    }
}

export default App;
